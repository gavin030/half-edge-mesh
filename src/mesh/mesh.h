#ifndef MESH_H
#define MESH_H

#include "drawable.h";
#include "meshcomponents.h";

#include <QOpenGLContext>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <la.h>
#include <vector>
#include <iostream>
#include <QString>

class Mesh : public Drawable
{
public:
    std::vector<Vertex*> vertices;
    std::vector<HalfEdge*> edges;
    std::vector<Face*> faces;
    std::vector<GLuint> vert_idx;
    std::vector<glm::vec4> vert_pos;
    std::vector<glm::vec4> vert_norm;
    std::vector<glm::vec4> vert_col;

    Mesh(GLWidget277* context);
    ~Mesh();
    void create() override;
    void convertMeshData2VBOInfo();

    void convert2CubeMesh();
    void triangulateFace(int id);
    void edgeAddMidpoint(int id);

    Vertex* generateVertex();
    HalfEdge* generateHalfEdge();
    Face* generateFace();
};

#endif // MESH_H
