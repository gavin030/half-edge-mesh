#include "mesh.h"


Mesh::Mesh(GLWidget277* context)
    : Drawable(context), vertices(), edges(), faces(),
      vert_idx(), vert_pos(), vert_norm(), vert_col()
{}

Mesh::~Mesh()
{
    for(int i = 0; i < vertices.size(); i++){
        delete vertices[i];
    }
    vertices.clear();
    for(int i = 0; i < edges.size(); i++){
        delete edges[i];
    }
    edges.clear();
    for(int i = 0; i < faces.size(); i++){
        delete faces[i];
    }
    faces.clear();
}

void Mesh::convertMeshData2VBOInfo()
{
    vert_idx.clear();
    vert_pos.clear();
    vert_norm.clear();
    vert_col.clear();

    int index = 0;
    for(auto& face : faces){
        HalfEdge* startEdge = face->he;
        vert_pos.push_back(glm::vec4(startEdge->nextVert->pos, 1));
        vert_col.push_back(glm::vec4(face->rgb, 1));

        HalfEdge* currentEdge = startEdge->nextHe;
        vert_pos.push_back(glm::vec4(currentEdge->nextVert->pos, 1));
        vert_col.push_back(glm::vec4(face->rgb, 1));
        int i = index + 1;
        glm::vec4 norm = glm::vec4();

        while(currentEdge->nextHe != startEdge){
            vert_idx.push_back(index);
            vert_idx.push_back(i++);
            vert_idx.push_back(i);
            vert_pos.push_back(glm::vec4(currentEdge->nextHe->nextVert->pos, 1));
            if(norm == glm::vec4()) norm = glm::vec4(glm::normalize(glm::cross(currentEdge->nextVert->pos - startEdge->nextVert->pos,
                                                                               currentEdge->nextHe->nextVert->pos - startEdge->nextVert->pos)), 0);
            vert_norm.push_back(norm);
            vert_col.push_back(glm::vec4(face->rgb, 1));

            currentEdge = currentEdge->nextHe;
        }
        vert_norm.push_back(norm);
        vert_norm.push_back(norm);
        index += 4;
    }
//    std::cout<<"vert_idx "<<vert_idx.size()<<std::endl;
//    std::cout<<"vert_pos "<<vert_pos.size()<<std::endl;
//    std::cout<<"vert_norm "<<vert_norm.size()<<std::endl;
//    std::cout<<"vert_col "<<vert_col.size()<<std::endl;
}

void Mesh::create()
{
    convertMeshData2VBOInfo();

    count = vert_idx.size();

    // Create a VBO on our GPU and store its handle in bufIdx
    generateIdx();
    // Tell OpenGL that we want to perform subsequent operations on the VBO referred to by bufIdx
    // and that it will be treated as an element array buffer (since it will contain triangle indices)
    mp_context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    mp_context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, vert_idx.size() * sizeof(GLuint), vert_idx.data(), GL_STATIC_DRAW);

    // The next few sets of function calls are basically the same as above, except bufPos and bufNor are
    // array buffers rather than element array buffers, as they store vertex attributes like position.
    generatePos();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_pos.size() * sizeof(glm::vec4), vert_pos.data(), GL_STATIC_DRAW);

    generateNor();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufNor);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_norm.size() * sizeof(glm::vec4), vert_norm.data(), GL_STATIC_DRAW);

    generateCol();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufCol);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_col.size() * sizeof(glm::vec4), vert_col.data(), GL_STATIC_DRAW);
}

void Mesh::convert2CubeMesh()
{
    for(int i = 0; i < vertices.size(); i++){
        delete vertices[i];
    }
    vertices.clear();
    for(int i = 0; i < edges.size(); i++){
        delete edges[i];
    }
    edges.clear();
    for(int i = 0; i < faces.size(); i++){
        delete faces[i];
    }
    faces.clear();

    Vertex::vertId = 0;
    HalfEdge::heId = 0;
    Face::faceId = 0;

    for(float x : {0.5, -0.5}){
        for(float y : {0.5, -0.5}){
            for(float z : {0.5, -0.5}){
                Vertex* v = generateVertex();
                v->pos = glm::vec3(x, y, z);
//                v->id = Vertex::vertId;
//                Vertex::vertId++;
//                v->setText(QString::number(v->id));
//                vertices.push_back(v);
            }
        }
    }

//    int edgeStartIndex;

    int face2Vertices[6][4] = {{0, 4, 5, 1},//frontFace
                               {6, 2, 3, 7},//backFace
                               {4, 6, 7, 5},//leftFace
                               {2, 0, 1, 3},//rightFace
                               {2, 6, 4, 0},//topFace
                               {1, 5, 7, 3}};//bottomFace
    std::vector<glm::vec3> face2Color = {glm::vec3(1, 0, 0),
                                         glm::vec3(0, 1, 0),
                                         glm::vec3(0, 0, 1),
                                         glm::vec3(1, 1, 0),
                                         glm::vec3(1, 0, 1),
                                         glm::vec3(0, 1, 1)};
//    std::vector<glm::vec3> face2Color = {glm::vec3(1, 1, 1),
//                                         glm::vec3(1, 1, 1),
//                                         glm::vec3(1, 1, 1),
//                                         glm::vec3(1, 1, 1),
//                                         glm::vec3(1, 1, 1),
//                                         glm::vec3(1, 1, 1)};

    for(int index = 0; index < 6; index++){
        Face* face = generateFace();
//        face->id = Face::faceId;
//        Face::faceId++;
//        face->setText(QString::number(face->id));
        face->rgb = face2Color[index];
//        faces.push_back(face);
        int edgeStartIndex = edges.size();
        for(int i : face2Vertices[index]){
            HalfEdge* edge = generateHalfEdge();
            edge->nextVert = vertices[i];
            vertices[i]->he = edge;
            edge->face = face;
//            edge->id = HalfEdge::heId;
//            HalfEdge::heId++;
//            edge->setText(QString::number(edge->id));
//            edges.push_back(edge);
        }
        for(int i = edgeStartIndex; i < edgeStartIndex+4; i++){
            edges[i]->nextHe = edges[(i+1-edgeStartIndex)%4 + edgeStartIndex];
        }
        face->he = edges[edgeStartIndex];
    }

    int face2NeighborFaces[6][4] = {{3, 4, 2, 5},//frontFace
                                    {2, 4, 3, 5},//backFace
                                    {0, 4, 1, 5},//leftFace
                                    {1, 4, 0, 5},//rightFace
                                    {3, 1, 2, 0},//topFace
                                    {3, 0, 2, 1}};//bottomFace
    int face2FaceEdges[6][4] = {{2, 3, 0, 1},//frontFace
                                {2, 1, 0, 3},//backFace
                                {2, 2, 0, 2},//leftFace
                                {2, 0, 0, 0},//rightFace
                                {1, 1, 1, 1},//topFace
                                {3, 3, 3, 3}};//bottomFace

    for(int index = 0; index < 6; index++){
        edges[4*index + 0]->symHe = edges[4*face2NeighborFaces[index][0] + face2FaceEdges[index][0]];
        edges[4*index + 1]->symHe = edges[4*face2NeighborFaces[index][1] + face2FaceEdges[index][1]];
        edges[4*index + 2]->symHe = edges[4*face2NeighborFaces[index][2] + face2FaceEdges[index][2]];
        edges[4*index + 3]->symHe = edges[4*face2NeighborFaces[index][3] + face2FaceEdges[index][3]];
    }
}

void Mesh::triangulateFace(int id)
{
    Face* face = faces[id];
    HalfEdge* startEdge = face->he;

    while(startEdge->nextHe->nextHe->nextHe != startEdge){
        HalfEdge* heA = generateHalfEdge();
        heA->nextVert = startEdge->nextVert;
        heA->nextVert->he = heA;
        HalfEdge* heB = generateHalfEdge();
        heB->nextVert = startEdge->nextHe->nextHe->nextVert;
        heB->nextVert->he = heB;
        heA->symHe = heB;
        heB->symHe = heA;
        heB->face = startEdge->face;

        Face* newFace = generateFace();
        newFace->rgb = startEdge->face->rgb;
        heA->face = newFace;
        startEdge->nextHe->face = newFace;
        startEdge->nextHe->nextHe->face = newFace;
        newFace->he = heA;

        heB->nextHe = startEdge->nextHe->nextHe->nextHe;
        startEdge->nextHe->nextHe->nextHe = heA;
        heA->nextHe = startEdge->nextHe;
        startEdge->nextHe = heB;
    }
}

void Mesh::edgeAddMidpoint(int id)
{
    HalfEdge* he1 = edges[id];
    HalfEdge* he2 = he1->symHe;

    Vertex* newVert = generateVertex();
    newVert->pos = (he1->nextVert->pos + he2->nextVert->pos) / 2.f;

    HalfEdge* he1b = generateHalfEdge();
    he1b->nextVert = he1->nextVert;
    he1b->face = he1->face;
    HalfEdge* he2b = generateHalfEdge();
    he2b->nextVert = he2->nextVert;
    he2b->face = he2->face;

    he1b->nextHe = he1->nextHe;
    he2b->nextHe = he2->nextHe;
    he1->nextHe = he1b;
    he2->nextHe = he2b;
    he1->nextVert = newVert;
    he2->nextVert = newVert;
    newVert->he = he2;
    he1->symHe = he2b;
    he2b->symHe = he1;
    he2->symHe = he1b;
    he1b->symHe = he2;
}

Vertex* Mesh::generateVertex()
{
    Vertex* vert = new Vertex();
    vert->id = Vertex::vertId++;
    vert->setText(QString::number(vert->id));
    vertices.push_back(vert);

    return vert;
}

HalfEdge* Mesh::generateHalfEdge()
{
    HalfEdge* he = new HalfEdge();
    he->id = HalfEdge::heId++;
    he->setText(QString::number(he->id));
    edges.push_back(he);

    return he;
}

Face* Mesh::generateFace()
{
    Face* face = new Face();
    face->id = Face::faceId++;
    face->setText(QString::number(face->id));
    faces.push_back(face);

    return face;
}
