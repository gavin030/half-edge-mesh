#ifndef DRAWABLEVERTEX_H
#define DRAWABLEVERTEX_H

#include "drawable.h"
#include "mesh/meshcomponents.h"

class DrawableVertex : public Drawable
{
public:
    bool inUse;
    Vertex* vert;
    std::vector<GLuint> vert_idx;
    std::vector<glm::vec4> vert_pos;
    std::vector<glm::vec4> vert_col;

    DrawableVertex(GLWidget277* context);
    void create() override;
    GLenum drawMode() override;

    void convertMeshData2VBOInfo();
};

#endif // DRAWABLEVERTEX_H
