#include "drawableedge.h"

DrawableEdge::DrawableEdge(GLWidget277* context)
    : Drawable(context), inUse(false), he(nullptr), vert_idx(),
      vert_pos(), vert_col()
{}

GLenum DrawableEdge::drawMode()
{
    return GL_LINES;
}

void DrawableEdge::convertMeshData2VBOInfo()
{
    vert_idx.clear();
    vert_pos.clear();
    vert_col.clear();

    vert_idx.push_back(0);
    vert_idx.push_back(1);

    vert_pos.push_back(glm::vec4(he->symHe->nextVert->pos, 1));
    vert_pos.push_back(glm::vec4(he->nextVert->pos, 1));

    vert_col.push_back(glm::vec4(1, 0, 0, 1));//start point red
    vert_col.push_back(glm::vec4(1, 1, 0, 1));//end point yellow


//    HalfEdge* startEdge = face->he;
//    vert_pos.push_back(glm::vec4(startEdge->nextVert->pos, 1));
//    vert_col.push_back(glm::vec4(glm::vec3(1, 1, 1) - face->rgb, 1));

//    HalfEdge* currentEdge = startEdge->nextHe;
//    vert_pos.push_back(glm::vec4(currentEdge->nextVert->pos, 1));
//    vert_col.push_back(glm::vec4(glm::vec3(1, 1, 1) - face->rgb, 1));
//    int i = 1;
//    glm::vec4 norm = glm::vec4();

//    while(currentEdge->nextHe != startEdge){
//        vert_idx.push_back(0);
//        vert_idx.push_back(i++);
//        vert_idx.push_back(i);
//        vert_pos.push_back(glm::vec4(currentEdge->nextHe->nextVert->pos, 1));
//        if(norm == glm::vec4()) norm = glm::vec4(glm::normalize(glm::cross(currentEdge->nextVert->pos - startEdge->nextVert->pos,
//                                                                           currentEdge->nextHe->nextVert->pos - startEdge->nextVert->pos)), 0);
//        vert_norm.push_back(norm);
//        vert_col.push_back(glm::vec4(face->rgb, 1));

//        currentEdge = currentEdge->nextHe;
//    }
//    vert_norm.push_back(norm);
//    vert_norm.push_back(norm);
}

void DrawableEdge::create()
{
    convertMeshData2VBOInfo();

    count = vert_idx.size();

    // Create a VBO on our GPU and store its handle in bufIdx
    generateIdx();
    // Tell OpenGL that we want to perform subsequent operations on the VBO referred to by bufIdx
    // and that it will be treated as an element array buffer (since it will contain triangle indices)
    mp_context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    mp_context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, vert_idx.size() * sizeof(GLuint), vert_idx.data(), GL_STATIC_DRAW);

    // The next few sets of function calls are basically the same as above, except bufPos and bufNor are
    // array buffers rather than element array buffers, as they store vertex attributes like position.
    generatePos();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_pos.size() * sizeof(glm::vec4), vert_pos.data(), GL_STATIC_DRAW);

    generateCol();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufCol);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_col.size() * sizeof(glm::vec4), vert_col.data(), GL_STATIC_DRAW);
}
