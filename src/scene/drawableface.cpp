#include "drawableface.h"

DrawableFace::DrawableFace(GLWidget277* context)
    : Drawable(context), inUse(false), face(nullptr), vert_idx(),
      vert_pos(), vert_col()
{}

GLenum DrawableFace::drawMode()
{
    return GL_LINES;
}

void DrawableFace::convertMeshData2VBOInfo()
{
    vert_idx.clear();
    vert_pos.clear();
    vert_col.clear();

    //GL_LINES draw mode is different from GL_TRIANGLES
    //when fill idxs, its 01, 12, 23 , .. instead of 012, 123, ...
    //needn't to set normals
    //still one color per vertex
    HalfEdge* startEdge = face->he;
    HalfEdge* lastEdge = face->he;

    int sideNum = 0;
    do{
        lastEdge = lastEdge->nextHe;
        sideNum++;
    }while(lastEdge != startEdge);

    lastEdge = face->he;

    glm::vec4 col = glm::vec4(glm::vec3(1, 1, 1) - face->rgb, 1);
    int i = 0;
    do{
        vert_idx.push_back(i);
        vert_idx.push_back((i + 1) % sideNum);
        vert_pos.push_back(glm::vec4(lastEdge->nextVert->pos, 1));
        vert_col.push_back(col);
        lastEdge = lastEdge->nextHe;
        i++;
    }while(lastEdge != startEdge);
}

void DrawableFace::create()
{
    convertMeshData2VBOInfo();

    count = vert_idx.size();

    // Create a VBO on our GPU and store its handle in bufIdx
    generateIdx();
    // Tell OpenGL that we want to perform subsequent operations on the VBO referred to by bufIdx
    // and that it will be treated as an element array buffer (since it will contain triangle indices)
    mp_context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    mp_context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, vert_idx.size() * sizeof(GLuint), vert_idx.data(), GL_STATIC_DRAW);

    // The next few sets of function calls are basically the same as above, except bufPos and bufNor are
    // array buffers rather than element array buffers, as they store vertex attributes like position.
    generatePos();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_pos.size() * sizeof(glm::vec4), vert_pos.data(), GL_STATIC_DRAW);

    generateCol();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufCol);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_col.size() * sizeof(glm::vec4), vert_col.data(), GL_STATIC_DRAW);
}
