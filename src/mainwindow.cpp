#include "mainwindow.h"
#include <ui_mainwindow.h>
#include "cameracontrolshelp.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mygl->setFocus();

//    std::cout<<ui->faceListWidget->currentItem()->text().toStdString()<<std::end;

//    connect(ui->mygl, SIGNAL(faceListSignal(std::vector<QListWidgetItem*>)), this, SLOT(faceListSlot(std::vector<QListWidgetItem*>)));
    connect(ui->mygl, SIGNAL(vertexSignal(QListWidgetItem*)), this, SLOT(vertexSlot(QListWidgetItem*)));
    connect(ui->mygl, SIGNAL(edgeSignal(QListWidgetItem*)), this, SLOT(edgeSlot(QListWidgetItem*)));
    connect(ui->mygl, SIGNAL(faceSignal(QListWidgetItem*)), this, SLOT(faceSlot(QListWidgetItem*)));
    connect(ui->vertexListWidget, SIGNAL(itemClicked(QListWidgetItem*)), ui->mygl, SLOT(vertexSelectedSlot(QListWidgetItem*)));
    connect(ui->vertexListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(myglFocusSlot()));
    connect(ui->edgeListWidget, SIGNAL(itemClicked(QListWidgetItem*)), ui->mygl, SLOT(edgeSelectedSlot(QListWidgetItem*)));
    connect(ui->edgeListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(myglFocusSlot()));
    connect(ui->faceListWidget, SIGNAL(itemClicked(QListWidgetItem*)), ui->mygl, SLOT(faceSelectedSlot(QListWidgetItem*)));
    connect(ui->faceListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(myglFocusSlot()));
    connect(ui->mygl, SIGNAL(myglVertexPosSignal(glm::vec3)), this, SLOT(mainwindowVertexPosSlot(glm::vec3)));
    connect(ui->mygl, SIGNAL(myglFaceColorSignal(glm::vec3)), this, SLOT(mainwindowFaceColorSlot(glm::vec3)));
    connect(ui->xDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(xVertexPosSlot(double)));
    connect(ui->xDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->yDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(yVertexPosSlot(double)));
    connect(ui->yDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->zDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(zVertexPosSlot(double)));
    connect(ui->zDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->rDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(rFaceColorSlot(double)));
    connect(ui->rDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->gDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(gFaceColorSlot(double)));
    connect(ui->gDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->bDoubleSpinBox, SIGNAL(valueChanged(double)), ui->mygl, SLOT(bFaceColorSlot(double)));
    connect(ui->bDoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(myglFocusSlot()));
    connect(ui->facetriangulatePushButton, SIGNAL(clicked(bool)), this, SLOT(mainwindowFaceTriangulateSlot()));
    connect(ui->edgeaddmidpointPushButton, SIGNAL(clicked(bool)), this, SLOT(mainwindowEdgeAddMidpointSlot()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::myglFocusSlot(QListWidgetItem* clicked)
//{
//    ui->mygl->setFocus();
//}

void MainWindow::myglFocusSlot()
{
    ui->mygl->setFocus();
}

void MainWindow::vertexSlot(QListWidgetItem *vert)
{
    ui->vertexListWidget->addItem(vert);
}

void MainWindow::edgeSlot(QListWidgetItem *he)
{
    ui->edgeListWidget->addItem(he);
}

void MainWindow::faceSlot(QListWidgetItem *face)
{
    ui->faceListWidget->addItem(face);
}

void MainWindow::mainwindowVertexPosSlot(glm::vec3 pos)
{
    ui->xDoubleSpinBox->blockSignals(true);
    ui->yDoubleSpinBox->blockSignals(true);
    ui->zDoubleSpinBox->blockSignals(true);
    emit ui->xDoubleSpinBox->setValue(pos[0]);
    emit ui->yDoubleSpinBox->setValue(pos[1]);
    emit ui->zDoubleSpinBox->setValue(pos[2]);
    ui->xDoubleSpinBox->blockSignals(false);
    ui->yDoubleSpinBox->blockSignals(false);
    ui->zDoubleSpinBox->blockSignals(false);
}

void MainWindow::mainwindowFaceColorSlot(glm::vec3 col)
{
    ui->rDoubleSpinBox->blockSignals(true);
    ui->gDoubleSpinBox->blockSignals(true);
    ui->bDoubleSpinBox->blockSignals(true);
    emit ui->rDoubleSpinBox->setValue(col[0]);
    emit ui->gDoubleSpinBox->setValue(col[1]);
    emit ui->bDoubleSpinBox->setValue(col[2]);
    ui->rDoubleSpinBox->blockSignals(false);
    ui->gDoubleSpinBox->blockSignals(false);
    ui->bDoubleSpinBox->blockSignals(false);
}

void MainWindow::mainwindowFaceTriangulateSlot()
{
    if(ui->faceListWidget->selectedItems().count() != 1) return;
    emit ui->mygl->myglFaceTriangulateSlot(ui->faceListWidget->selectedItems().at(0));
}

void MainWindow::mainwindowEdgeAddMidpointSlot()
{
    if(ui->edgeListWidget->selectedItems().count() != 1) return;
    emit ui->mygl->myglEdgeAddMidpointSlot(ui->edgeListWidget->selectedItems().at(0));
}



void MainWindow::on_actionQuit_triggered()
{
    QApplication::exit();
}

void MainWindow::on_actionCamera_Controls_triggered()
{
    CameraControlsHelp* c = new CameraControlsHelp();
    c->show();
}

